mkdir c:\github
winget install --id=Git.Git -e --accept-package-agreements --accept-source-agreements
winget install --id=Microsoft.VisualStudioCode -e  
winget install --id=AgileBits.1Password -e
winget install --id=7zip.7zip -e
winget install --id=File-New-Project.EarTrumpet -e  
winget install --id=TimKosse.FileZilla.Client -e  
winget install --id=BlastApps.FluentSearch -e  
REM winget install "Flow Launcher"
REM winget install --id=Microsoft.GitCredentialManagerCore -e  
REM winget install --id=GitHub.cli -e  
REM winget install --id=GitHub.GitHubDesktop -e  
REM winget install --id=Google.Chrome -e 
winget install -e --id Mozilla.Firefox 
winget install --id=Greenshot.Greenshot -e  
REM winget install -e --id ShareX.ShareX
winget install --id=Microsoft.dotnet -e  
winget install --id=Microsoft.Teams -e  
winget install -e --id Ferdium.Ferdium
winget install --id=OpenJS.NodeJS.LTS -e  
winget install --id=JanDeDobbeleer.OhMyPosh -e  
winget install --id=Microsoft.PowerShell -e  
winget install --id=Microsoft.PowerToys -e  
winget install --id=Python.Python.3 -e  
winget install --id=Microsoft.RemoteDesktopClient -e  
winget install --id=NickeManarin.ScreenToGif -e  
winget install --id=Spotify.Spotify -e   
REM winget install --id=Canonical.Ubuntu -e  
REM winget install --id=dorssel.usbipd-win -e  
REM winget install --id=Microsoft.VisualStudio.2022.Community -e  
winget install --id=VideoLAN.VLC -e  
winget install --id=WhatsApp.WhatsApp -e  
winget install -e --id RocketChat.RocketChat
winget install -e --id Telegram.TelegramDesktop
winget install -e --id Discord.Discord
winget install -e --id SlackTechnologies.Slack
winget install --id=WinMerge.WinMerge -e  
winget install -e --id Obsidian.Obsidian
winget install -e --id mRemoteNG.mRemoteNG
REM winget install -e --id voidtools.Everything
winget install -e --id flux.flux
winget install -e --id QL-Win.QuickLook
winget install HermannSchinagl.LinkShellExtension
winget install -e --id LupoPenSuite.DropIt
winget install -e --id Insynchq.Insync
winget install -e --id VideoLAN.VLC
winget install -e --id WinSCP.WinSCP

